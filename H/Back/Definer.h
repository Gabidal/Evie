#ifndef _DEFINER_H_
#define _DEFINER_H_
#include "Token.h"
#include "../Lexer/Lexer.h"
#include "../Lexer/Component.h"
#include <vector>
#include <string>

            //_// This is where we try to define what are the types defined in source code.
            //_// And make them into reasonable tokens.

class Definer
{
private:
	 // nej
    // Mörkönenä was here. 20:09 thu 7.11.19;
public:
    vector<Token*> Input_Of_Tokens;
    vector<Token*> Defined_Types;
    vector<Token*> Output;
    string Get_All(vector<string> x, string add);
    int Get_Location_Of_Type_Constructor(string type);
    string Get_Definition_Setting(Token* t, string f);
    void Get_Modded_Content(string dest, string source);
    void Type_Collect(Token* t);
    Token* FIND(string name);
    bool Has(Token* t, string s);
    void Initialize_Global_Variables();
    void Factory();
    Definer()
    {
    }
    
    ~Definer(){}
};



#endif