#include "../../H/Back/Generator.h"
#include "../../H/Selector/Selector.h"
extern Selector* S;
extern vector<string> Pre_Defined_Tokens;
extern int _SYSTEM_BIT_TYPE;
bool Double_Tasking = false;
int Personalize = 0;

void Generator::Factory()
{
	for ( int i = 0; i < Input.size(); i++)
		Hide_Un_Used_Function(i);
	for (int i = 0; i < (int)Input.size(); i++)
	{
		Detect_Assembly_Including(Input.at(i));
		//Detect_Prefixes(Input.at(i));
		Hide_Real_Size(Input.at(i));
		Detect_Pointters(Input.at(i));
		Detect_Arrays(Input.at(i));
		Detect_Address_Pointing(Input.at(i));
		Detect_Condition(Input.at(i));
		Detect_Function(Input.at(i));
		Detect_Operator(Input.at(i));
		Detect_Parenthesis(Input.at(i));
		Detect_Pre_Defined_Tokens(Input.at(i));
		//keep this last always last!!
		Initialize_Global_Variable(i);
	}
}

void Generator::Detect_Function(Token* t)
{
	//if (t->is("type"))
	//	return;
	if (t->is(_Constructor_))
	{
		//make a label OpC*
		Detect_Prefixes(t);
		IR* ir = new IR;
		ir->ID = "label";
		if (t->is("mangle")) {
			ir->PreFix = Mangler::Mangle(t);
			ir->ID = "raw_label";
		}
		else
			ir->PreFix = t->Name;
		ir->add(_Start_Of_Label);
		Double_Tasking = false;
		//make the stackfrmae
		if (t->is(_Need_For_Space_))
		{
			Double_Tasking = true;
			//save ebp
			//stack base
			Token* ebp = new Token;
			ebp->Name = "ebp";
			ebp->Size = _SYSTEM_BIT_TYPE;
			ebp->add(_Register_);
			ebp->add(Task_For_Type_Address_Basing);
			//mov ebp, esp
			IR* push = new IR;
			push->ID = "push";
			push->Parameters.push_back(ebp);
			ir->Childs.push_back(push);
			//load the atm stack head
			//stack head
			Token* esp = new Token;
			esp->Name = "esp";
			esp->Size = _SYSTEM_BIT_TYPE;
			esp->add(_Register_);
			esp->add(Task_For_Type_Address);
			//mov ebp, esp
			IR* movebpesp = new IR;
			movebpesp->ID = "ldr";
			movebpesp->Parameters.push_back(ebp);
			movebpesp->Parameters.push_back(esp);
			ir->Childs.push_back(movebpesp);
			if (t->Reservable_Size > 0)
			{
				//make the register
				Token* esp = new Token;
				esp->add(_Register_);
				esp->add(Task_For_Type_Address);
				esp->Name = "reserve memory please!";
				esp->Size = _SYSTEM_BIT_TYPE;
				//make the number to subtract from esp
				Token* num = new Token;
				num->Name = to_string(t->Reservable_Size);
				num->Size = _SYSTEM_BIT_TYPE;
				num->add(_Number_);
				//make the IR token
				IR* space = new IR;
				space->ID = "-";
				space->Parameters.push_back(esp);
				space->Parameters.push_back(num);
				ir->Childs.push_back(space);
			}
		}
		//make the insights of this constructor
		Generator g;
		g.Input.push_back(t->Right_Side_Token);
		g.Types = this->Types;
		g.Factory();
		//get the output as the new IR's childs.
		Append(&ir->Childs, g.Output);
		//make the ending label
		IR* end = new IR;
		end->ID = "label";
		if (t->is("mangle")) {
			end->PreFix = Mangler::Mangle(t) + "END";
			end->ID = "raw_label";
		}
		else
			end->PreFix = t->Name + "END";
		end->add(_End_Of_Label);
		Output.push_back(ir);
		Output.push_back(end);
	}
	else if (t->is(_Call_))
	{
		//unstable way to get the state from types list
		Token* Constructor = nullptr;
		/*for (Token* i : Types)
			if (i->Name == t->Name) {
				if (i->Left_Side_Token->Childs.size() != t->Left_Side_Token->Childs.size()) continue;
				bool Right_Constructor = true;
				for (int j = 0; j < i->Left_Side_Token->Childs.size(); j++)
					if (i->Left_Side_Token->Childs[j]->Size != t->Left_Side_Token->Childs[j]->Size) Right_Constructor = false;
				if (Right_Constructor) {
					t->State = i->State;
					Constructor = i;
				}
			}*/
		for (Token* i : Types)
			if (i->Name == t->Name) {
				bool Right_Constructor = true;
				if (t->is("mangle")) {
					if (i->Left_Side_Token->Childs.size() != t->Left_Side_Token->Childs.size()) continue;
					for (int x = 0; x < i->Left_Side_Token->Childs.size(); x++) {
						if (t->Left_Side_Token->Childs[x]->is(_Number_) || t->Left_Side_Token->Childs[x]->is("cache")) {
							bool Parameter_Is_Decimal = false;
							bool Template_Parameter_Is_Decimal = false;
							//cheking if other is decimal and other is not
							if (t->Left_Side_Token->Childs[x]->is(_Number_) && find(t->Left_Side_Token->Childs[x]->Name.begin(), t->Left_Side_Token->Childs[x]->Name.end(), '.') != t->Left_Side_Token->Childs[x]->Name.end())
								Parameter_Is_Decimal = true;
							else if (t->Left_Side_Token->Childs[x]->State == "decimal")
								Parameter_Is_Decimal = true;
							if (i->Left_Side_Token->Childs[x]->State == "decimal")
								Template_Parameter_Is_Decimal = true;
							if (Parameter_Is_Decimal ^ Template_Parameter_Is_Decimal) {
								//if neither is then good, if other is and other not its bad, if both are its good
								Right_Constructor = false;
								break;
							}
							if (i->Left_Side_Token->Childs[x]->Size != t->Left_Side_Token->Childs[x]->Size) {
								Right_Constructor = false;
								break;
							}
						}
						else {
							if (i->Left_Side_Token->Childs[x]->Types.size() != t->Left_Side_Token->Childs[x]->Types.size()) {
								Right_Constructor = false;
								break;
							}
							for (int y = 0; y < i->Left_Side_Token->Childs[x]->Types.size(); y++)
								if (i->Left_Side_Token->Childs[x]->Types[y] != t->Left_Side_Token->Childs[x]->Types[y]) {
									Right_Constructor = false;
									break;
								}
						}
					}
				}
				if (Right_Constructor) {
					t->State = i->State;
					Constructor = i;
					break;
				}
			}
		if (t->is("mangle") && Constructor == nullptr) {
			cout << "Error: Could not find suitable constructor to call " << t->Name << "(";
			for (int i = 0; i < t->Left_Side_Token->Childs.size()-1; i++) {
				cout << "[" << t->Left_Side_Token->Childs[i]->Get_Types() << "]" << t->Left_Side_Token->Childs[i]->Name << " ,";
			}
			cout << "[" << t->Left_Side_Token->Childs[t->Left_Side_Token->Childs.size() - 1]->Get_Types() << "]" << t->Left_Side_Token->Childs[t->Left_Side_Token->Childs.size() - 1]->Name << ")" << endl;
			exit(1);
		}
		//make a callation OpC*
		IR* ir = new IR;
		ir->ID = "call";
		//when we give the token call, (name) to it and in parser defined as
		//_External_ it can do something like this: call [banana] ; as a global variable.
		t->ID = Personalize++;
		if (t->is("mangle"))
			t->Name = Mangler::Mangle(Constructor);
		else
			t->Name = t->Name;
		ir->Parameters.push_back(t);

		for (Token* t : t->Left_Side_Token->Childs)
		{
			Generator g;
			g.Types = Types;
			g.Input.push_back(t);
			g.Factory();

			Append(&Output, g.Output);

			IR* push = new IR;
			push->ID = "push";

			if (g.Handle != nullptr) {
				push->Parameters.push_back(g.Handle);
			}
			else {
				push->Parameters.push_back(t);
			}
			Output.push_back(push);
		}
		//generate the parameters as IR tokens
		//Generator g;
		//g.Input.push_back(t->Left_Side_Token);
		//g.Types = this->Types;
		//g.Factory();
		//get the output from the generator and store then into the parent IR operator.
		//ir->Childs = g.Output;
		//make better reverser so that ur pointter loading wont be also reversed XD!!
		//reverse(g.Output.begin(), g.Output.end());
		//Append(&Output, g.Output);


		Output.push_back(ir);
		//handle
		Token* T = new Token;
		T->add(_Register_);
		T->add(Task_For_Returning);
		T->Name = t->Name;
		T->ID = t->ID;
		T->Size = _SYSTEM_BIT_TYPE;
		Handle = T;
		//check if this has init some objects so that we can reserve stack for it.
		if ((t->Reservable_Size > 0) && !t->is("loyal"))
		{
			//make the register
			Token* esp = new Token;
			esp->add(_Register_);
			esp->add(Task_For_Type_Address);
			esp->Name = "reserve memory please!";
			esp->Size = _SYSTEM_BIT_TYPE;
			//make the number to subtract from esp
			Token* num = new Token;
			num->Name = to_string(t->Reservable_Size);
			num->Size = _SYSTEM_BIT_TYPE;
			num->add(_Number_);
			//make the IR token
			IR* space = new IR;
			space->ID = "+";
			space->Parameters.push_back(esp);
			space->Parameters.push_back(num);
			Output.push_back(space);
		}
	}
}

void Generator::Detect_Condition(Token* t)
{
	//if (condition is not what wanted then jump into end of if)
	//(
	//	insides of condition if condition is true
	//)
	//looping condition if loop token
	//end of condition
	if (t->is(_Condition_))
	{
		//make the starting label token as a label pointter.
		IR* Condition = new IR;
		Condition->ID = "label";
		Condition->PreFix = t->Name + to_string(t->ID);
		Condition->add(_Start_Of_Label);
		//make IR tokens for condition.
		Generator g;
		g.Types = this->Types;
		g.Input.clear();
		g.Input.push_back(t->Left_Side_Token);
		g.Factory();
		//the back end for architecture specific allocates-
		//more IR tokens for example "CMP" and for the JMP condition.
		//So this is just a straight line of intermadiate-
		//assembly tokens that we can optimize and affect and etc...
		Condition->Childs = g.Output;
		//Now we need the Insides of the condition to be placed into here:
		g.Input.clear();
		g.Output.clear();
		g.Input.push_back(t->Right_Side_Token);
		g.Factory();
		Append(&Condition->Childs, g.Output);
		//Check if this condition need for repeating itself.
		if (t->Name == "while")
		{
			//this jumps straigt to start of the condition loop
			IR* Loop = new IR;
			Loop->PreFix = "jmp";
			Loop->ID = t->Name + to_string(t->ID);
			Condition->Childs.push_back(Loop);
		}
		//Now we need to make the exit label for the condition.
		IR* Exit_Label = new IR;
		Exit_Label->PreFix = t->Name + to_string(t->ID) + "END";
		Exit_Label->ID = "label";
		Exit_Label->add(_End_Of_Label);
		Output.push_back(Condition);
		Output.push_back(Exit_Label);
	}
}

void Generator::Scaler(Token* l, Token* r)
{
	if (l->is(_Number_))
	{
		if (l->Size < r->Size)
		{
			l->Size = r->Size;
		}
	}
	if (r->is(_Number_))
	{
		if (r->Size < l->Size)
		{
			r->Size = l->Size;
		}
	}
	if (r->_Dynamic_Size_)
	{
		r->Size = l->Size;
	}
	if (l->_Dynamic_Size_)
	{
		l->Size = r->Size;
	}
}

void Generator::Dodge(Token* l, Token* r)
{
	if (l->is(_Call_) && r->is(_Call_))
	{
		//create the Right side now and give the eax into ebx for dodging the left side calling
		Generator g;
		g.Input.push_back(new Token(*r));
		g.Types = Types;
		g.Factory();

		//save the calling
		Append(&Output, g.Output);

		//make the saving register
		Token* reg = new Token;
		reg->Name = r->Name + "_Saving_" + to_string(r->ID);
		reg->add(Task_For_General_Purpose);
		reg->add(_Register_);
		reg->Size = _SYSTEM_BIT_TYPE;

		//now g.Handle has the EAX
		//now save it into someother register
		IR* save = new IR;
		save->ID = "ldr";
		save->Parameters.push_back(reg);
		save->Parameters.push_back(g.Handle);
		Output.push_back(save);
		//now overwrite the existing calling convension by the newly made register
		*r = *reg;
	}
}

void Generator::Rotator(Token* l, Token* r)
{
	if (!l->is(_Number_))
		return;

}

void Generator::Initialize_Global_Variable(int i)
{
	Token* t = Input.at(i);
	if (t->is(_Initialized_))
	{
		Scaler(t, t->Initial_Value);
		IR* init = new IR;
		t->Initial_Value->add(_Locked_);
		init->Parameters.push_back(t->Initial_Value);
		if (t->Initial_Value->is(_String_))
		{
			init->ID = "makestring";
		}
		else if (t->Initial_Value->Size == 1)
		{
			init->ID = "db";
		}
		else if (t->Initial_Value->Size == 2)
		{
			init->ID = "dw";
		}
		else if (t->Initial_Value->Size == 4)
		{
			init->ID = "dd";
		}
		else if (t->Initial_Value->Size == 8)
		{
			init->ID = "dq";
		}
		else if (t->Initial_Value->Size == 12)
		{
			init->ID = "dt";
		}
		IR* label = new IR;
		label->PreFix = t->Name;
		label->ID = "label";
		label->Childs.push_back(init);
		Output.push_back(label);
		Input.erase(Input.begin() + i);

		if (i < Input.size())
		{
			Token* t = Input.at(i);
			if (t->is(_Initialized_))
				Initialize_Global_Variable(i);
		}
	}
}

void Generator::Detect_Operator(Token* t)
{
	if (t->is(_Operator_) != true)
		return;
	if (t->is(_Generated_) == true)
		return;
	t->add(_Generated_);
	// a = a + 1 + a
	// mov eax, [a]
	// cvsi2sd xmm0, eax
	// mov [a], xmm0
	//basic tools:
	Token* R = nullptr;
	Token* L = nullptr;
	if (t->Right_Side_Token->is(_Operator_))
		R = t->Right_Non_Operative_Token;
	else 
		R = t->Right_Side_Token;
	if (t->Left_Side_Token->is(_Operator_))
		L = t->Left_Non_Operative_Token;
	else 
		L = t->Left_Side_Token;

	Hide_Real_Size(R);
	Hide_Real_Size(L);

	Scaler(R, L);

	Dodge(L, R);


	Token* Left_Token = new Token;
	Token* Right_Token = new Token;
	bool Normal_Left = false;
	bool Normal_Right = false;
	//check if this is a storing opcode:
	bool Storing = (t->Name == "=" || t->Name == "str");
	//set the leftside token to inherit the rightside tokens register
	if (Storing)
		t->Left_Side_Token->Name_Of_Same_Using_Register = t->Right_Side_Token->Name;
	//now do the same but for right side.
	Generator g;
	g.Types = Types;
	g.Input.push_back(t->Right_Side_Token);
	g.Factory();
	//save the information gived by the generator yet again.
	Append(&Output, g.Output);
	//check if right side has more complex instruction.
	if (g.Handle != nullptr)
		Right_Token = g.Handle;
	//if normal right side.
	else if(!(t->Right_Side_Token->is(_Register_)))
		Normal_Right = true;
	else
	{
		Right_Token = t->Right_Side_Token;
	}
	//create a new IR token.
	IR* opCode = new IR;
	opCode->ID = t->Name;
	opCode->add(_Operator_);
	//give the new ir generator the left side of operation.
	g.Output.clear();
	g.Input.clear();
	g.Handle = nullptr;
	g.Input.push_back(t->Left_Side_Token);
	g.Factory();
	//save the information that the new generator gived.
	Append(&Output, g.Output);
	//check if left side holds a more complex instruction for loading into a register.
	if (g.Handle != nullptr)
		Left_Token = g.Handle;
	//if not then probably just a normal number/variable
	else if (!(t->Left_Side_Token->is(_Register_)))
		Normal_Left = true;
	else
	{
		Left_Token = t->Left_Side_Token;
	}
	//b:0 = a:0
	//lea edi, [(ebp-offset)+ecx*size]
	if ((Normal_Right && !t->Right_Side_Token->is(_Number_)) || (Normal_Right &&  t->Right_Side_Token->Size >= 8))
	{
		//make a handle register
		Token* Reg = new Token;
		Reg->add(Task_For_General_Purpose);
		Reg->add(_Register_);
		Reg->Name = "Reg2_" + t->Right_Side_Token->Name;
		Reg->Size = t->Right_Side_Token->Size;
		Reg->StackOffset = t->StackOffset;
		Right_Token = Reg;
		//make the loading IR token
		IR* load = new IR;
		load->ID = "ldr";
		load->Parameters.push_back(new Token(*Reg));
		load->Parameters.push_back(new Token(*t->Right_Side_Token));
		Output.push_back(load);
	}
	else if (t->Right_Side_Token->is(_Number_)){
		Right_Token = t->Right_Side_Token;
	}
	if (Normal_Left)
	{
		if (Storing)
		{
			Left_Token = t->Left_Side_Token;
			//this is for the optimized cache usation.
			Left_Token->Name_Of_Same_Using_Register = Right_Token->Name;
			//now skip the "=" for cache users
			if (Left_Token->is("cache"))
				opCode->ID = "ldr";
		}
		else
		{
			//make a handle register
			Token* Reg = new Token;
			Reg->add(Task_For_General_Purpose);
			Reg->add(_Register_);
			Reg->Name = "Reg1_" + t->Left_Side_Token->Name;
			Reg->Size = t->Left_Side_Token->Size;
			Reg->StackOffset = t->StackOffset;
			Left_Token = Reg;
			//make the loading IR token
			IR* load = new IR;
			load->ID = "ldr";
			load->Parameters.push_back(new Token(*Reg));
			load->Parameters.push_back(new Token(*t->Left_Side_Token));
			Output.push_back(load);
			//more check if the destination is too big for the loaded register, in Emulator
		}
		/*else if (!t->Left_Side_Token->is(_Number_))
		{
			//make a handle register
			Token* Reg = new Token;
			Reg->add(Task_For_General_Purpose);
			Reg->add(_Register_);
			Reg->Name = "Reg1_" + t->Left_Side_Token->Name;
			Reg->Size = t->Left_Side_Token->Size;
			Reg->StackOffset = t->StackOffset;
			Left_Token = Reg;
			//make the loading IR token
			IR* load = new IR;
			load->ID = "ldr";
			load->Parameters.push_back(new Token(*Reg));
			load->Parameters.push_back(new Token(*t->Left_Side_Token));
			Output.push_back(load);
			//more check if the destination is too big for the loaded register, in Emulator
		}
		else if (t->Left_Side_Token->is(_Number_)){
			Left_Token = t->Left_Side_Token;
		}*/
	}
	Rotator(Left_Token, Right_Token);
	opCode->Parameters.push_back(new Token(*Left_Token));
	opCode->Parameters.push_back(new Token(*Right_Token));
	Handle = Left_Token;
	Output.push_back(opCode);
}

void Generator::Detect_Pointters(Token* t)
{
	//a::0 --> mov reg, [a] --> mov reg2, [reg+0]
	//a::0 == _Pointing_

	//a:0 --> mov reg, [a+0]
	//a:0 == _Array_
	//if (t->is(_Giving_Address_))
	//	return;
	if (t->Offsetter == nullptr)
		return;
	if (!t->is(_Pointting_))
		return;
	Token* tmp_t = new Token;
	*tmp_t = *t;
	//empty the offsetter from tmp
	tmp_t->Offsetter = nullptr;
	Token* Offsetter = new Token;

	Scaler(t, t->Offsetter);
	//load the Offsetter into a reg or if number stay number.
	Generator g;
	g.Input.clear();
	g.Output.clear();
	g.Types = Types;

	g.Input.push_back(t->Offsetter);
	g.Factory();
	Append(&Output, g.Output);
	if (g.Handle != nullptr)
		//a::(a:b) --> more complex
		*Offsetter = *g.Handle;
	else if (!t->Offsetter->is(_Number_) && !t->Offsetter->is("cache")) {
		//a::a --> more simpler offsetter
		//setup the offsetter token to be the handle for the variable loading
		Offsetter->add(_Register_);
		Offsetter->add(Task_For_General_Purpose);
		Offsetter->Name = t->Offsetter->Name + "_Offsetted_Handle_Reg";
		Offsetter->StackOffset = t->StackOffset;
		Offsetter->Size = _SYSTEM_BIT_TYPE; //!!!!!dont trust !!!!
		//load the variable into a rgister
		IR* load = new IR;
		load->ID = "ldr";
		load->Parameters.push_back(new Token(*Offsetter));
		load->Parameters.push_back(new Token(*t->Offsetter));
		Output.push_back(load);
	}
	else
		//a::0 --> just a number offsetter in other words no need to do enything.
		*Offsetter = *t->Offsetter;
	//now load the main variable:
	Token* Offsetter_Register = new Token;
	Offsetter_Register->add(_Register_);
	Offsetter_Register->add(Task_For_General_Purpose);
	Offsetter_Register->Name = t->Name + "_Offsetter_Reg";
	Offsetter->StackOffset = t->StackOffset;
	Offsetter_Register->Size = t->Size;
	Offsetter_Register->Hidden_Size = t->Hidden_Size;
	//Offsetter_Register->Name_Of_Same_Using_Register = t->Name_Of_Same_Using_Register;

	//mov sec_reg, [a]
	IR* load_Secondary = new IR;
	load_Secondary->ID = "ldr";
	//make a copy so that when we add the offsetter it doesnt explode :D
	load_Secondary->Parameters.push_back(new Token(*Offsetter_Register));
	load_Secondary->Parameters.push_back(new Token(*tmp_t));

	Output.push_back(load_Secondary);

	//now load the main, from secondary handle + offsetter
	//mov reg, [sec_reg+offsetter]
	Token* Main_Handle = new Token;
	Main_Handle->add(_Register_);
	if (t->Name_Of_Same_Using_Register != "")
		Main_Handle->add(Task_For_Dest_Offsetting);
	else
		Main_Handle->add(Task_For_Source_Offsetting);
	Main_Handle->Name = t->Name + "_main_Handle_reg";
	Main_Handle->StackOffset = t->StackOffset;
	Main_Handle->Size = t->Size;



	Offsetter_Register->Offsetter = Offsetter;
	Offsetter_Register->add(_Pointting_);


	//if the main parent is a parameter the children arent so lets give em
	if (t->is(_Parameter_))
		Offsetter_Register->add(_Parameter_);

	//make the initial mov
	IR* Main_Load = new IR;
	if (t->Name_Of_Same_Using_Register != "" || t->is(_Giving_Address_))
		Main_Load->ID = ":";
	else
		Main_Load->ID = "ldr";
	Main_Load->Parameters.push_back(new Token(*Main_Handle));
	Main_Load->Parameters.push_back(new Token(*Offsetter_Register));

	Output.push_back(Main_Load);

	Handle = Main_Handle;
	return;
	/*
	//now the offsetters main registers value has been loaded and just give it back.
	Offsetter_Register->add(t->get());
	Offsetter_Register->Offsetter = t->Offsetter;
	*t = *Offsetter_Register;
	//test giving the handle:
	Handle = t;
	//also give offsetter the giving address flag that above code loses.
	*/
}

void Generator::Detect_Arrays(Token* t)
{
	//a:0 --> mov reg, [(ebp-4) + offsetter]
	if (t->is(_Giving_Address_))
		return;
	if (!t->is(_Array_))
		return;
	if (t->Offsetter == nullptr)
		return;
	if (t->_Has_Member_)
		return;
	//make the offsetter handle
	Token* Offsetter = new Token;

	Scaler(t, t->Offsetter);
	//load the Offsetter into a reg or if number stay number.
	Generator g;
	g.Input.clear();
	g.Output.clear();
	g.Types = Types;

	g.Input.push_back(t->Offsetter);
	g.Factory();
	Append(&Output, g.Output);
	if (g.Handle != nullptr)
		//a:(a:b) --> more complex
		*Offsetter = *g.Handle;
	else if (!t->Offsetter->is(_Number_) && !t->is("cache")) {
		//a:a --> more simpler offsetter
		//setup the offsetter token to be the handle for the variable loading
		Offsetter->add(_Register_);
		Offsetter->add(Task_For_General_Purpose);
		Offsetter->Name = t->Offsetter->Name + "_Offsetted_Handle_Reg";
		Offsetter->Size = _SYSTEM_BIT_TYPE; //!!!!! dont trust!!!!
		Offsetter->StackOffset = t->StackOffset;
		//load the variable into a rgister
		IR* load = new IR;
		load->ID = "ldr";
		load->Parameters.push_back(Offsetter);
		load->Parameters.push_back(t->Offsetter);
		Output.push_back(load);
	}
	else
		//a:0 --> just a number offsetter in other words no need to do enything.
		*Offsetter = *t->Offsetter;

	//set the original offsetter into the new one
	t->Offsetter = Offsetter;
	if (t->Name_Of_Same_Using_Register == ""){
		//make the returning handle register
		Token* Main_Handle = new Token;
		Main_Handle->add(_Register_);
		if (t->Name_Of_Same_Using_Register != "")
			Main_Handle->add(Task_For_Dest_Offsetting);
		else 
			Main_Handle->add(Task_For_Source_Offsetting);
		Main_Handle->Size = t->Size;
		Main_Handle->Name = t->Name + "_Main_Handle";

		//make the main load
		IR* Main_Load = new IR;
		Main_Load->ID = "ldr";
		Main_Load->Parameters.push_back(Main_Handle);
		Main_Load->Parameters.push_back(new Token(*t));

		Output.push_back(Main_Load);

		Handle = Main_Handle;
	}
}

void Generator::Detect_Address_Pointing(Token* t)
{
	//@a
	if (t->is(_Giving_Address_) != true) return;
	if (t->is(_Pointting_)) return;
	//if (t->Name_Of_Same_Using_Register != "") return;
	//lea eax, [ebp -4 + ecx]
	//eax
	//make a token reg for to handle future of the pointers usage.
	Token* Reg = new Token;
	Reg->Name = t->Name + "_Giving_Address_regiser";
	Reg->Size = _SYSTEM_BIT_TYPE;
	//Reg->Size = t->Size;					!!!!!fix this!!!!!
	Reg->add(Task_For_General_Purpose);
	Reg->add(_Register_);
	Reg->StackOffset = t->StackOffset;

	//_:ebx , _24[ebp  - 4]
	//this no good,
	//give *t system bit size or else ill kill u
	t->Size = _SYSTEM_BIT_TYPE;

	IR* lea = new IR;
	lea->ID = ":";
	lea->Parameters.push_back(new Token(*Reg));
	lea->Parameters.push_back((new Token(*t)));
	*t = *Reg;

	Output.push_back(lea);

	Handle = Reg;
}

void Generator::Detect_Parenthesis(Token* t)
{
	if (t->is(_Parenthesis_) && t->Childs.size() > 0)
	{
		Generator g;
		g.Input = t->Childs;
		g.Types = this->Types;
		g.Factory();
		Append(&Output, g.Output);
		Handle = g.Handle;
		if ((t->Childs.size() == 1) && (t->Childs.at(0)->is(_Operator_) == false) && (Handle == nullptr))
		{
			Handle = t->Childs.at(0);
		}
	}
}

void Generator::Detect_Pre_Defined_Tokens(Token* t)
{
	for (string T : Pre_Defined_Tokens)
	{
		if (t->is(T))
		{
			IR *ir = new IR;
			ir->ID = T;
			Generator g;
			g.Types = Types;
			if (t->Right_Side_Token != nullptr) {
				g.Input.push_back(t->Right_Side_Token);
				g.Factory();
				Append(&Output, g.Output);
				if (g.Handle != nullptr)
				{
					ir->Parameters.push_back(g.Handle);
				}
				else
				{
					ir->Parameters.push_back(t->Right_Side_Token);
				}
			}
			if (Double_Tasking)
				ir->add(_Double_Task_);
			Output.push_back(ir);
			return;
		}
	}
}

void Generator::Detect_Prefixes(Token* t)
{
	for (string s: t->Types) {
		if (s == "hidden" || t->State == "hidden")
			return;
		//only put those types here that we WANT to SKIP!
		if (s == "type" || s == "func" || s == "loyal" || s == "mangle" || t->State == "type" || t->State == "func" || t->State == "loyal" )//(Lexer::GetComponents(s).back().is(KEYWORD_COMPONENT) || Lexer::GetComponents(t->State).back().is(KEYWORD_COMPONENT))		 ..
			continue;
		IR* ir = new IR;
		ir->PreFix = s;
		ir->ID = t->Name;
		Output.push_back(ir);
	}
}

void Generator::Detect_Assembly_Including(Token* t)
{
	if (!t->_IS_ASM_INCLUDE_)
		return;
	//make an IR token that uses the selections %include ID
	IR* Include = new IR;
	Include->ID = "include";
	//t as an parameter as an string
	t->add(_String_);
	t->_RAW_STRING_ = true;
	Include->Parameters.push_back(t);
	Output.push_back(Include);
	return;
}

void Generator::Append(vector<IR*> *Dest, vector<IR*> Source)
{
	for (IR* i : Source)
	{
		Dest->push_back(i);
	}
}

void Generator::Hide_Un_Used_Function(int i)
{
	//the update of callation_count is made in parser.cpp
	//find the f from types list where the updattors updates the count of function callation
	//if the callation amount is 0 then delete this function, exept for EXPORT typed ones.
	if (i >= Input.size())
		return;
	Token* f = Input.at(i);
	if (!f->is(_Constructor_))
		return;
	if (f->is("export")  || f->Name == "main")	// || f->is("type")
		return;
	int count = 0;
	for (Token* t: Types)
		if (f->Name == t->Name) {
			count = t->Callation_Count;
			break;
		}
	if (count == 0) {
		Input.erase(Input.begin() + i);
		if (i < Input.size() && Input.at(i)->is(_Constructor_))
			Hide_Un_Used_Function(i);
	}
	return;
}

void Generator::Hide_Real_Size(Token* t)
{
	if (t->Hidden_Size > 0)
		return;
	if (t->is("ptr")){
		t->Hidden_Size = t->Size;
		t->Size = _SYSTEM_BIT_TYPE;
	}
	return;
}
